export interface UrlSchema {
    url: string;
    protocol: string;
    port: string;
    hostname: string;
    pathname: string;
    search: string;
    query: any;
    hash: string;
}
/**
 * 解析url的 queryString ,  foo=bar&search=123  => {foo: 'bar', search: '123'}
 * @param str string
 * @return object
 */
export declare const queryStringParse: (str: string) => any;
/**
* 生成queryString , {foo: 'bar', search: 123}  => foo=bar&search=123
* @param data object
* @return string
*/
export declare const queryStringify: (data: any) => string;
/**
 * 解析url
 * @param url string
 * @return object
 */
export declare const parse: (url?: string) => UrlSchema;
/**
 * 拼接url和query参数
 * @param url string
 * @param data object
 * @return string
 */
export declare const append: (url: string, data: any) => string;
