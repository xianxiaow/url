import { isUndef, isEmptyObject, isObject } from 'tiny-is';
/**
 * 解析url的 queryString ,  foo=bar&search=123  => {foo: 'bar', search: '123'}
 * @param str string
 * @return object
 */
export var queryStringParse = function (str) {
    var ret = {};
    str.replace(/^\?/, '').split('&').forEach(function (item) {
        var s = item.split('=');
        var key = s[0];
        ret[key] = decodeURIComponent(s[1]);
    });
    return ret;
};
/**
* 生成queryString , {foo: 'bar', search: 123}  => foo=bar&search=123
* @param data object
* @return string
*/
export var queryStringify = function (data) {
    var ret = [];
    for (var k in data) {
        var value = encodeURIComponent(data[k]);
        ret.push(k + "=" + value);
    }
    return ret.join('&');
};
/**
 * 解析url
 * @param url string
 * @return object
 */
export var parse = function (url) {
    if (isUndef(url)) {
        url = window.location.href;
    }
    if (!/^(https|http):\/\/[^\s]+/.test(url)) {
        var ret = {};
        return ret;
    }
    var a = document.createElement('a');
    a.href = url;
    var search = a.search.replace(/^\?/, '');
    return {
        url: url,
        protocol: a.protocol.replace(':', ''),
        port: a.port,
        hostname: a.hostname,
        pathname: a.pathname.replace(/^([^\/])/, '/$1'),
        search: search,
        query: queryStringParse(search),
        hash: a.hash.replace('#', '')
    };
};
/**
 * 拼接url和query参数
 * @param url string
 * @param data object
 * @return string
 */
export var append = function (url, data) {
    if (isObject(data) && !isEmptyObject(data)) {
        return url + '?' + queryStringify(data);
    }
    return url;
};
