import { isUndef, isEmptyObject, isObject } from 'tiny-is';

export interface UrlSchema{
  url: string;
  protocol: string,
  port: string,
  hostname: string,
  pathname: string,
  search: string,
  query: any,
  hash: string
}

/**
 * 解析url的 queryString ,  foo=bar&search=123  => {foo: 'bar', search: '123'}
 * @param str string
 * @return object
 */
export const queryStringParse = (str: string) => {
  const ret: any = {};
  str.replace(/^\?/, '').split('&').forEach((item) => {
    const s = item.split('=');
    const key = s[0];
    ret[key] = decodeURIComponent(s[1]);
  });
  return ret;
}

/**
* 生成queryString , {foo: 'bar', search: 123}  => foo=bar&search=123
* @param data object
* @return string
*/
export const queryStringify = (data) => {
  const ret = [];
  for (let k in data) {
    const value = encodeURIComponent(data[k])
    ret.push(`${k}=${value}`)
  }
  return ret.join('&');
}

/**
 * 解析url
 * @param url string
 * @return object
 */
export const parse = (url?: string): UrlSchema => {
  if (isUndef(url)) {
    url = window.location.href;
  }
  if (!/^(https|http):\/\/[^\s]+/.test(url)) {
    const ret: any = {};
    return ret;
  }
  const a = document.createElement('a');
  a.href = url;

  const search = a.search.replace(/^\?/, '');
  return {
    url,
    protocol: a.protocol.replace(':', ''),
    port: a.port,
    hostname: a.hostname,
    pathname: a.pathname.replace(/^([^\/])/, '/$1'),
    search,
    query: queryStringParse(search),
    hash: a.hash.replace('#', '')
  };
}

/**
 * 拼接url和query参数
 * @param url string
 * @param data object
 * @return string
 */
export const append = (url: string, data) => {
  if (isObject(data) && !isEmptyObject(data)) {
    return url + '?' + queryStringify(data);
  }
  return url;
}
